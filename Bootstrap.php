<?php declare(strict_types = 1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl_backenduser_extension
 */

namespace Plugin\jtl_backenduser_extension;

use Plugin\jtl_backenduser_extension\helpers\BackendAccount;
use JTL\Events\Dispatcher;
use JTL\Plugin\Bootstrapper;
use JTL\Shop;

/**
 * Class Bootstrap
 * @package Plugin\jtl_backenduser_extension
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @inheritDoc
     */
    public function boot(Dispatcher $dispatcher): void
    {
        parent::boot($dispatcher);

        $dispatcher->listen('shop.hook.' . \HOOK_NEWS_PAGE_DETAILANSICHT, function (array $args) {
            if (isset($args['newsItem'])) {
                BackendAccount::getInstance($this->getPlugin())->hydrateNews([$args['newsItem']]);
            }
        });

        $dispatcher->listen('shop.hook.' . \HOOK_NEWS_PAGE_NEWSUEBERSICHT, function (array $args) {
            if (isset($args['items'])) {
                BackendAccount::getInstance($this->getPlugin())->hydrateNews($args['items']->all());
            }
        });

        $dispatcher->listen('shop.hook.' . \HOOK_GET_NEWS, function (array $args) {
            if (isset($args['oNews_arr'])) {
                BackendAccount::getInstance($this->getPlugin())->hydrateNews($args['oNews_arr']->all());
            }
        });

        $dispatcher->listen('shop.hook.' . \HOOK_BACKEND_ACCOUNT_EDIT, function (array $args) {
            if (isset($args['type'])) {
                switch ($args['type']) {
                    case 'VALIDATE':
                        $args['result'] = BackendAccount::getInstance($this->getPlugin())->validateAccount(
                            $args['oAccount'],
                            $args['attribs'],
                            $args['messages']
                        );
                        break;
                    default:
                        $args['result'] = true;
                        break;
                }
            }
        });

        $dispatcher->listen('shop.hook.' . \HOOK_BACKEND_ACCOUNT_PREPARE_EDIT, function (array $args) {
            $args['content'] = BackendAccount::getInstance($this->getPlugin())->getContent(
                $args['oAccount'],
                $args['smarty'],
                $args['attribs']
            );
        });

        $dispatcher->listen('shop.hook.' . \HOOK_BACKEND_FUNCTIONS_GRAVATAR, static function (array $args) {
            if (isset($args['AdminAccount'])) {
                /** @var \stdClass $adminExt */
                $adminExt = $args['AdminAccount'];
                $url      = Shop::Container()->getDB()->select(
                    'tadminloginattribut',
                    'kAdminlogin',
                    $adminExt->kAdminlogin,
                    'cName',
                    'useAvatarUpload',
                    null,
                    null,
                    false,
                    'cAttribValue'
                );

                $args['url'] = !empty($url->cAttribValue) ? $url->cAttribValue : $args['url'];
            }
        });
    }
}
