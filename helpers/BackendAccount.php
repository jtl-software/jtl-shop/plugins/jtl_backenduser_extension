<?php declare(strict_types = 1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl_backenduser_extension
 */

namespace Plugin\jtl_backenduser_extension\helpers;

use JTL\ContentAuthor;
use JTL\Language\LanguageHelper;
use JTL\News\Item;
use JTL\Plugin\PluginInterface;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use stdClass;
use StringHandler;

/**
 * Class BackendAccount
 * @package Plugin\jtl_backenduser_extension\helpers
 */
class BackendAccount
{
    /**
     * @var PluginInterface
     */
    private $extension;

    /**
     * @var static
     */
    private static $instance;

    /**
     * @param PluginInterface $plugin
     * @return static
     */
    public static function getInstance(PluginInterface $plugin): self
    {
        if (static::$instance === null) {
            static::$instance = new self($plugin);
        }

        return static::$instance;
    }

    /**
     * BackendAccountHelper constructor.
     * @param PluginInterface $plugin
     */
    private function __construct(PluginInterface $plugin)
    {
        $this->extension = $plugin;
    }

    /**
     * @param array $tmpFile
     * @param string $attribName
     * @return mixed bool|string
     */
    private function uploadImage(array $tmpFile, string $attribName)
    {
        $imgType = \array_search($tmpFile['type'][$attribName], [
            \IMAGETYPE_JPEG => \image_type_to_mime_type(\IMAGETYPE_JPEG),
            \IMAGETYPE_PNG  => \image_type_to_mime_type(\IMAGETYPE_PNG),
            \IMAGETYPE_BMP  => \image_type_to_mime_type(\IMAGETYPE_BMP),
            \IMAGETYPE_GIF  => \image_type_to_mime_type(\IMAGETYPE_GIF),
        ], true);

        if ($imgType !== false) {
            $imagePath = \PFAD_MEDIA_IMAGE . 'avatare/';
            $imageName = \pathinfo($tmpFile['name'][$attribName], \PATHINFO_FILENAME)
                . \image_type_to_extension($imgType);
            if (\is_dir(\PFAD_ROOT . $imagePath) || \mkdir(\PFAD_ROOT . $imagePath, 0755)) {
                if (\move_uploaded_file($tmpFile['tmp_name'][$attribName], \PFAD_ROOT . $imagePath . $imageName)) {
                    return '/' . $imagePath . $imageName;
                }
            }
        }

        return false;
    }

    /**
     * @param string $relURL
     * @return string
     */
    private function getFullImageURL(string $relURL): string
    {
        return Shop::getImageBaseURL() . \ltrim($relURL, '/');
    }

    /**
     * @param $imagePath
     */
    private function deleteImage(string $imagePath): void
    {
        if (\is_file(\PFAD_ROOT . $imagePath)) {
            \unlink(\PFAD_ROOT . $imagePath);
        }
    }

    /**
     * @param string $paramName
     * @param string|null $defaultValue
     * @return string|null
     */
    public function getConfigParam($paramName, $defaultValue = null)
    {
        return $this->extension->getConfig()->getValue($paramName) ?? $defaultValue;
    }

    /**
     * @param array $contentArr
     */
    public function hydrateNews(array $contentArr): void
    {
        foreach ($contentArr as $newsItem) {
            /** @var Item $newsItem */
            $author = ContentAuthor::getInstance()->getAuthor('NEWS', $newsItem->getID(), true);

            if (!isset($author->kAdminlogin) || $author->kAdminlogin <= 0) {
                continue;
            }
            // Avatar benutzen?
            if (isset($author->extAttribs['useAvatar'])) {
                if ($this->getConfigParam('use_avatar', 'N') === 'Y') {
                    if ($author->extAttribs['useAvatar']->cAttribValue === 'G') {
                        $params = ['email' => null, 's' => 80, 'd' => 'mm', 'r' => 'g'];
                        $url    = 'https://www.gravatar.com/avatar/';
                        $mail   = empty($author->extAttribs['useGravatarEmail']->cAttribValue)
                                ? $author->cMail
                                : $author->extAttribs['useGravatarEmail']->cAttribValue;
                        $url   .= \md5(\mb_convert_case(\trim($mail), MB_CASE_LOWER));
                        $url   .= '?' . \http_build_query($params, '', '&');

                        $author->cAvatarImgSrc     = $url;
                        $author->cAvatarImgSrcFull = $url;
                    }
                    if ($author->extAttribs['useAvatar']->cAttribValue === 'U') {
                        $author->cAvatarImgSrc     = $author->extAttribs['useAvatarUpload']->cAttribValue;
                        $author->cAvatarImgSrcFull = $this->getFullImageURL(
                            $author->extAttribs['useAvatarUpload']->cAttribValue
                        );
                    }
                } else {
                    $author->extAttribs['useAvatar']->cAttribValue = 'N';
                }
            }
            unset($author->extAttribs['useAvatarUpload'], $author->extAttribs['useGravatarEmail']);

            // Vita benutzen?
            if (isset($author->extAttribs['useVita_' . $_SESSION['cISOSprache']])
                && $this->getConfigParam('use_vita', 'N') === 'Y'
            ) {
                $author->cVitaShort = $author->extAttribs['useVita_' . $_SESSION['cISOSprache']]->cAttribValue;
                $author->cVitaLong  = $author->extAttribs['useVita_' . $_SESSION['cISOSprache']]->cAttribText;
            }
            foreach (LanguageHelper::getAllLanguages() as $language) {
                unset($author->extAttribs['useVita_' . $language->cISO]);
            }

            // Google+ benutzen?
            if (!empty($author->extAttribs['useGPlus']->cAttribValue)
                && $this->getConfigParam('use_gplus', 'N') === 'Y'
            ) {
                $author->cGplusProfile = $author->extAttribs['useGPlus']->cAttribValue;
            }
            unset($author->extAttribs['useGPlus']);
            $newsItem->setAuthor($author);
        }
    }

    /**
     * @param array $contentArr
     * @param string $realm
     * @param string $contentKey
     */
    public function getFrontend($contentArr, $realm, $contentKey): void
    {
        if (!\is_array($contentArr)) {
            return;
        }
        foreach ($contentArr as $key => $content) {
            $author = ContentAuthor::getInstance()->getAuthor($realm, $content->$contentKey, true);

            if (!isset($author->kAdminlogin) || $author->kAdminlogin <= 0) {
                continue;
            }
            // Avatar benutzen?
            if (isset($author->extAttribs['useAvatar']) && $this->getConfigParam('use_avatar', 'N') === 'Y') {
                if ($author->extAttribs['useAvatar']->cAttribValue === 'G') {
                    $params = ['email' => null, 's' => 80, 'd' => 'mm', 'r' => 'g'];
                    $url    = 'https://www.gravatar.com/avatar/';
                    $mail   = empty($author->extAttribs['useGravatarEmail']->cAttribValue)
                        ? $author->cMail
                        : $author->extAttribs['useGravatarEmail']->cAttribValue;
                    $url   .= \md5(\mb_convert_case(\trim($mail), MB_CASE_LOWER));
                    $url   .= '?' . \http_build_query($params, '', '&');

                    $author->cAvatarImgSrc     = $url;
                    $author->cAvatarImgSrcFull = $url;
                }
                if ($author->extAttribs['useAvatar']->cAttribValue === 'U') {
                    $author->cAvatarImgSrc     = $author->extAttribs['useAvatarUpload']->cAttribValue;
                    $author->cAvatarImgSrcFull = $this->getFullImageURL(
                        $author->extAttribs['useAvatarUpload']->cAttribValue
                    );
                }
            } elseif (isset($author->extAttribs['useAvatar'])) {
                $author->extAttribs['useAvatar']->cAttribValue = 'N';
            }
            unset($author->extAttribs['useAvatarUpload'], $author->extAttribs['useGravatarEmail']);

            // Vita benutzen?
            if (isset($author->extAttribs['useVita_' . $_SESSION['cISOSprache']])
                && $this->getConfigParam('use_vita', 'N') === 'Y'
            ) {
                $author->cVitaShort = $author->extAttribs['useVita_' . $_SESSION['cISOSprache']]->cAttribValue;
                $author->cVitaLong  = $author->extAttribs['useVita_' . $_SESSION['cISOSprache']]->cAttribText;
            }
            foreach (LanguageHelper::getAllLanguages() as $language) {
                unset($author->extAttribs['useVita_' . $language->cISO]);
            }

            // Google+ benutzen?
            if (!empty($author->extAttribs['useGPlus']->cAttribValue)
                && $this->getConfigParam('use_gplus', 'N') === 'Y'
            ) {
                $author->cGplusProfile = $author->extAttribs['useGPlus']->cAttribValue;
            }
            unset($author->extAttribs['useGPlus']);

            $contentArr[$key]->oAuthor = $author;
        }
    }

    /**
     * HOOK_BACKEND_ACCOUNT_PREPARE_EDIT
     *
     * @param stdClass $account
     * @param JTLSmarty $smarty
     * @param array     $attribs
     * @return string
     * @throws \Exception
     */
    public function getContent(stdClass $account, JTLSmarty $smarty, array $attribs): string
    {
        $showAvatar          = $this->getConfigParam('use_avatar', 'N') === 'Y';
        $showVita            = $this->getConfigParam('use_vita', 'N') === 'Y';
        $showGPlus           = $this->getConfigParam('use_gplus', 'N') === 'Y';
        $showSectionPersonal = $showAvatar || $showVita || $showGPlus;

        if ($showAvatar) {
            $gravatarEmail = '';
            if (!empty($attribs['useGravatarEmail']->cAttribValue)) {
                $gravatarEmail = $attribs['useGravatarEmail']->cAttribValue;
            } elseif (isset($account->cMail)) {
                $gravatarEmail = $account->cMail;
            }
            $uploadImage = isset($attribs['useAvatar']->cAttribValue)
            && $attribs['useAvatar']->cAttribValue === 'U'
            && !empty($attribs['useAvatarUpload']->cAttribValue)
                ? $attribs['useAvatarUpload']->cAttribValue
                : '/' . \BILD_UPLOAD_ZUGRIFF_VERWEIGERT;
        } else {
            $gravatarEmail = '';
            $uploadImage   = '';
        }

        $languages      = LanguageHelper::getAllLanguages();
        $defaultAttribs = [
            'useGravatarEmail' => (object)['cAttribValue' => ''],
            'useAvatar'        => (object)['cAttribValue' => ''],
            'useAvatarUpload'  => (object)['cAttribValue' => ''],
            'useGPlus'         => (object)['cAttribValue' => ''],
        ];
        foreach ($languages as $language) {
            $defaultAttribs['useVita_' . $language->cISO] = (object)[
                'cAttribValue' => '',
                'cAttribText'  => '',
            ];
        }
        $attribs = \array_merge($defaultAttribs, $attribs);

        $result = $smarty
            ->assign('oAccount', $account)
            ->assign('showAvatar', $showAvatar)
            ->assign('showVita', $showVita)
            ->assign('showGPlus', $showGPlus)
            ->assign('sectionPersonal', $showSectionPersonal)
            ->assign('gravatarEmail', $gravatarEmail)
            ->assign('uploadImage', $uploadImage)
            ->assign('attribValues', $attribs)
            ->assign('sprachen', $languages)
            ->fetch($this->extension->getPaths()->getAdminPath() . 'templates/userextension_index.tpl');

        return $result;
    }

    /**
     * HOOK_BACKEND_ACCOUNT_EDIT - VALIDATE
     *
     * @param stdClass $account
     * @param array     $attribs
     * @param array     $messages
     * @return mixed bool|array - true if success otherwise errormap
     */
    public function validateAccount(stdClass $account, array &$attribs, array &$messages)
    {
        $result = true;

        if ($this->getConfigParam('use_avatar', 'N') === 'Y') {
            if (!$attribs['useAvatar']) {
                $attribs['useAvatar'] = 'N';
            }

            switch ($attribs['useAvatar']) {
                case 'G':
                    if (!empty($attribs['useAvatarUpload'])) {
                        $this->deleteImage($attribs['useAvatarUpload']);
                        $attribs['useAvatarUpload'] = '';
                    }
                    break;
                case 'U':
                    $attribs['useGravatarEmail'] = '';

                    if (isset($_FILES['extAttribs']) && !empty($_FILES['extAttribs']['name']['useAvatarUpload'])) {
                        $attribs['useAvatarUpload'] = $this->uploadImage($_FILES['extAttribs'], 'useAvatarUpload');

                        if ($attribs['useAvatarUpload'] === false) {
                            $messages['error'] .= 'Fehler beim Bilupload!';

                            $result = ['useAvatarUpload' => 1];
                        }
                    } elseif (empty($attribs['useAvatarUpload'])) {
                        $messages['error'] .= 'Bitte geben Sie ein Bild an!';

                        $result = ['useAvatarUpload' => 1];
                    }

                    break;
                default:
                    $attribs['useGravatarEmail'] = '';

                    if (!empty($attribs['useAvatarUpload'])) {
                        $this->deleteImage($attribs['useAvatarUpload']);
                        $attribs['useAvatarUpload'] = '';
                    }
            }
        }

        if ($this->getConfigParam('use_vita', 'N') === 'Y') {
            foreach (LanguageHelper::getAllLanguages() as $language) {
                $useVita_ISO = 'useVita_' . $language->cISO;
                if (!empty($attribs[$useVita_ISO])) {
                    $shortText = StringHandler::filterXSS($attribs[$useVita_ISO]);
                    $longtText = $attribs[$useVita_ISO];

                    if (\mb_strlen($shortText) > 255) {
                        $shortText = \mb_substr($shortText, 0, 250) . '...';
                    }

                    $attribs[$useVita_ISO] = [$shortText, $longtText];
                }
            }
        }

        return $result;
    }
}
